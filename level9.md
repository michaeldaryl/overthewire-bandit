# Level 9

Commands used:
```
strings data.txt | grep '='
```

### Strategy
*strings* prints the strings of printable characters in files. We then use grep to find strings starting with '='.

### Key
truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
