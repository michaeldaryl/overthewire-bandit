# Level 8

Command used:
```
sort data.txt | uniq -u
```

### Strategy
Use sort to sort the files, uniq compares only adjacent lines that's why we need to use sort. Use the option -u to print only unique lines. By default, uniq reports or filters out repeated lines.

### Key 
UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
