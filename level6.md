# Level 6

Commands used:
```
find / -user bandit7 -group bandit6 -size 33c 2>/dev/null
```

### Strategy
Use "find" to find the file then redirect all errors to /dev/null

### Key
HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs
