# Level 3

Commands used:
```
cd inhere
ls -la
cat .hidden
```

### Strategy
Use "-la" option to ls to view hidden files.

### Key
pIwrPrtPN36QITSp3EQaw936yaFoFgAB
