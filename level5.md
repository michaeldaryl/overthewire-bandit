# Level 5

Commands used:
```
find ./inhere -type f -size 1033c -not -executable
cat ./inhere/maybehere07/.file2
```

### Strategy
Use find to search for the files. Provide the right options.

### Key
DXjZPULLxYr17uwoI01bNLQbtFemEgo7
